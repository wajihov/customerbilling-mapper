package org.openlab.dto;

import java.math.BigDecimal;
import java.util.Date;

import org.openlab.entities.Customer;

public class InvoiceResponseDTO {

	private String id;
	private Date date;
	private BigDecimal amount;
	private Customer customer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public InvoiceResponseDTO(String id, Date date, BigDecimal amount, Customer customer) {
		super();
		this.id = id;
		this.date = date;
		this.amount = amount;
		this.customer = customer;
	}

	public InvoiceResponseDTO() {
		super();
	}

}
