package org.openlab.services;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.openlab.dto.InvoiceRequestDTO;
import org.openlab.dto.InvoiceResponseDTO;
import org.openlab.entities.Customer;
import org.openlab.entities.Invoice;
import org.openlab.mapper.InvoiceMapper;
import org.openlab.openFeign.CustomerRestClient;
import org.openlab.repositories.InvoiceRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Configuration
public class InvoiceServiceImpl implements InvoiceService {

	private InvoiceRepository invoiceRepository;
	private InvoiceMapper invoiceMapper;
	private CustomerRestClient customerRestClient;

	public InvoiceServiceImpl(InvoiceRepository invoiceRepository, InvoiceMapper invoiceMapper,
			CustomerRestClient customerRestClient) {
		this.invoiceRepository = invoiceRepository;
		this.invoiceMapper = invoiceMapper;
		this.customerRestClient = customerRestClient;
	}

	@Override
	public InvoiceResponseDTO save(InvoiceRequestDTO invoiceRequestDTO) {
		Invoice invoice = invoiceMapper.fromInvoiceRequestDTO(invoiceRequestDTO);
		invoice.setId(UUID.randomUUID().toString());
		invoice.setDate(new Date());
		// vérification de l'intégrité référentielle Invoice / Custumer

		Invoice saveInvoice = invoiceRepository.save(invoice);
		return invoiceMapper.fromInvoice(saveInvoice);
	}

	@Override
	public InvoiceResponseDTO getInvoice(String invoiceId) {
		Invoice invoice = invoiceRepository.findById(invoiceId).get();
		Customer customer = customerRestClient.getCostumer(invoice.getCustomerId());
		invoice.setCustomer(customer);

		return invoiceMapper.fromInvoice(invoice);
	}

	@Override
	public List<InvoiceResponseDTO> invoiceByCustomerId(String customerId) {
		List<Invoice> invoices = invoiceRepository.findByCustomerId(customerId);
		return invoices.stream().map(invoice -> invoiceMapper.fromInvoice(invoice)).collect(Collectors.toList());
	}

}
