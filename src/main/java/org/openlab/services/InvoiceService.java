package org.openlab.services;

import java.util.List;

import org.openlab.dto.InvoiceRequestDTO;
import org.openlab.dto.InvoiceResponseDTO;

public interface InvoiceService {

	InvoiceResponseDTO save(InvoiceRequestDTO invoiceRequestDTO);

	InvoiceResponseDTO getInvoice(String invoiceId);

	List<InvoiceResponseDTO> invoiceByCustomerId(String customerId);

}
