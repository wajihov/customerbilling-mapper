package org.openlab.openFeign;

import java.util.List;

import org.openlab.entities.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "COSTUMER-SERVICE")
public interface CustomerRestClient {

	@GetMapping(path = "/api/customers/{id}")
	Customer getCostumer(@PathVariable String id);

	@GetMapping(path = "/api/customers")
	List<Customer> allCustumers();

}
