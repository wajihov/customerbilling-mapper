package org.openlab.web;

import java.util.List;

import org.openlab.dto.InvoiceRequestDTO;
import org.openlab.dto.InvoiceResponseDTO;
import org.openlab.services.InvoiceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class InvoiceRestController {

	private InvoiceService invoiceService;

	public InvoiceRestController(InvoiceService invoiceService) {
		super();
		this.invoiceService = invoiceService;
	}

	@GetMapping(path = "/getInvoices/{id}")
	public InvoiceResponseDTO getInvoice(@PathVariable String invoiceId) {
		return invoiceService.getInvoice(invoiceId);
	}

	@GetMapping(path = "/getInvoices/{customerId}")
	public List<InvoiceResponseDTO> getInvoicesByCustomer(@PathVariable String customerId) {
		return invoiceService.invoiceByCustomerId(customerId);
	}

	@PostMapping("/invoices")
	public InvoiceResponseDTO save(@RequestBody InvoiceRequestDTO invoiceRequestDTO) {
		return invoiceService.save(invoiceRequestDTO);
	}

}
