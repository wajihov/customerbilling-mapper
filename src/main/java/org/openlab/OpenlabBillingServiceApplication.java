package org.openlab;

import java.math.BigDecimal;

import org.openlab.dto.InvoiceRequestDTO;
import org.openlab.services.InvoiceService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableFeignClients
/*
 * @ComponentScan(basePackages = {
 * "org.openlab.OpenlabBillingServiceApplication", "org.openlab.services",
 * "org.openlab.mapper.InvoiceMapper" })
 */
//@ComponentScan({ "org.openlab.mapper.InvoiceMapper", "org.openlab.services" })
//@EnableAutoConfiguration
public class OpenlabBillingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenlabBillingServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(InvoiceService invoiceService) {
		return arg -> {
			System.out.println("Hi les amis ");

			invoiceService.save(new InvoiceRequestDTO(BigDecimal.valueOf(12345), "C01"));
			invoiceService.save(new InvoiceRequestDTO(BigDecimal.valueOf(45678), "C02"));
			invoiceService.save(new InvoiceRequestDTO(BigDecimal.valueOf(14725), "C04"));
			invoiceService.save(new InvoiceRequestDTO(BigDecimal.valueOf(98745), "C03"));
		};
	}
}
